8) Na sua interpretação:

Qual foi o nível de dificuldade da implementação da questão do bloco A?

> [ ] Muito Fácil

> [ ] Fácil

> [X] Médio

> [ ] Difícil

> [ ] Muito Difícil

b. Qual foi o nível de dificuldade para encontrar os erros do programa da questão 7?

> [ ] Muito Fácil

> [X] Fácil

> [ ] Médio

> [ ] Difícil

> [ ] Muito Difícil